####1. BALANCE AND ALIGNMENT
![](balance.png)
- Are the elements arranged so no one section is heavier or draws too much attention than the other?
- What kind of balance is used, and is it successful?
- Also recall, there are basically three kinds of balance in design, symmetrical, asymmetrical, and radial.
- The one you pick will help convey the mood of the piece.

####2. ALIGNMENT
![](alignment.png)
- Are the elements in the piece placed so that they line up on the page in an organized manner?
- Is the design attractive and is the text legible and readable?
- You will want to check both your vertical and horizontal alignment. Good alignment is invisible and means that every element is visually connected to another element
####3. REPITITION AND CONSISTENCY
![](repitition&consistency.png)
- Are certain elements repeated at consistent intervals or in the same position throughout the project?
- Is the piece laid out with a consistent look that readers can expect?
- Since one of the options is designing a business card, if you choose that option,
- Be sure to repeat design elements and use consistent type and graphic style within it to help viewers navigate your designs and layouts
####3. CONTRAST AND COLOR
![](contrast.png)
- How well does the contrast of colors work in the piece?
- Does the use of fonts emphasize what is important or direct the reader's eyes to what is important?
-  In design, big and small elements, black and white colors, squares and circle patterns can all create contrast.
-  The key to working with contrast is to make sure the differences are obvious.
####4. PROXIMITY AND SPACE
![](proximity&space.png)
- Are the objects near each other seen as a unit? Are objects on the page spaced in a unified manner?
- Also keep in mind the use of space, and try not to cram too much text and graphics onto the page.
- Remember, white space is your friend and gives you places to have breathing room, and
-  even quiet spaces for the user to take all the elements in in your piece.
