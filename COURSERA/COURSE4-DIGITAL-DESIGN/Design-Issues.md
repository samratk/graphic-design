<h4> DESIGN ISSUES </h4>

**`1. Absence of Visual Texture` 🡆 `absence  of hierarchy type faces, paragraph alignment`**

<img src="../../ILLUSTRATIONS/texture1.png" width="300"/> <img src="../../ILLUSTRATIONS/texture2.png" width="300"/> <img src="../../ILLUSTRATIONS/texture3.png" width="300"/>
<pre><b>
1. Separate headline and body copy
2. Adding images
3. Variant typefaces
4. Paragraph alignment
5. Break up the text with headlines and images that helps you to tell your story.
</b></pre>


**`2. Distracting Patterns` 🡆 `Caused due to issues in text spacing and wordings`**

<img src="../../ILLUSTRATIONS/pattern1.png" width="300"/> <img src="../../ILLUSTRATIONS/widow.png" width="300"/> <img src="../../ILLUSTRATIONS/texture3.png" width="300"/>
<pre><b>
1. Poor rag creates distraction. This can be solved by changing margins or putting manual new lines.
2. Avoid widows and orphans by kerning and tracking of characters.
3. Variant typefaces
4. Paragraph alignment
5. Break up the text with headlines and images that helps you to tell your story.
</b></pre>


**`3. Unclear focus or random use of direction` 🡆 `Solve it by adding balance or a pattern to guide the eyes`**

<img src="../../ILLUSTRATIONS/balance.png" width="300"/>
<pre><b>
1. Add balance by spreading and floating the images.
2. Do not overdo the images
</b></pre>


**`4. Disarray or lack of association` 🡆 `solve by unifying items; group similar information`**

<img src="../../ILLUSTRATIONS/disarray.png" width="300"/>
<pre><b>
1. Group similar things together as one piece.
</b></pre>
