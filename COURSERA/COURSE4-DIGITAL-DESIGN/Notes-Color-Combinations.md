<h4>COLOR COMBINATIONS</h4>

![72point.png](../../ILLUSTRATIONS/colors.png)
![72point.png](../../ILLUSTRATIONS/color1.png)
![72point.png](../../ILLUSTRATIONS/compcol.png)
![72point.png](../../ILLUSTRATIONS/split-comp.png)
![72point.png](../../ILLUSTRATIONS/triad.png)
![72point.png](../../ILLUSTRATIONS/analogous.png)
![72point.png](../../ILLUSTRATIONS/tetradic.png)
![72point.png](../../ILLUSTRATIONS/warm-cool.png)
![72point.png](../../ILLUSTRATIONS/summary.png)
