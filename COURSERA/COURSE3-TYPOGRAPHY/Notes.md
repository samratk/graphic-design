![cmyk](../../ILLUSTRATIONS/Typography.png)
![72point.png](../../ILLUSTRATIONS/hts.png)

1. Alignment
   1. Widows
   1. Orphans
1. Leading - inte baseline distance. 12 points size 🠊 15 leading.
![72point.png](../../ILLUSTRATIONS/leading.png)
2. Kerning - general space between letters. This is the spacing between the part that is leading or lagging the most from the letter besides it. 🠊 Equal Space
3. Tracking - the first touching of the letter on the baseline's track.  This is the spacing of the bottoms of the letters.  🠊 Optical Space  🠊 Negative Tracking - gloves fits in the hand.
![points](../../ILLUSTRATIONS/space.png)
![points](../../ILLUSTRATIONS/points.png)
![72point.png](../../ILLUSTRATIONS/72point.png)

1. Attention
2. Intention

1. FONTS
   1. Serif
      1. Old Style
         1. Garamond
         2. Goudy Old Style
         3. Palatino
         4. Times New Roman
      2. Neo Classic
         1. Century School book
         2. Bodoni
         3. Didot
         4. Baskerville
      3. Slab
         1. Rockwell
         2. Courier
         3. Cooper
   1. Sans Serif
      1. Grotesque
         1. Franklin Gothic
         2. Helvetica
         3. News Gothic
      1. Humanistic
         1. Gill Sans
         2. Verdana
         3. Corbel
      1. Geometric
         1. Futura
         2. Bauhaus
         3. Avenir
   1. Decorative
      1. Decorative
         1. Curlz
         2. Chalkboard
         3. Desdemona
      2. Casual Script
         1. Brush script
         2. Bradley Hand
         3. Noteworthy
      3. Formal Script
         1. Edwardian Script
         2. Apple Chancery
         3. Lucida Calligraphy
