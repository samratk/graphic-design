1. CMYK
![cmyk](../../ILLUSTRATIONS/CMYK.png)
2. Subtractive color model (Print Industry) - Pigment theory.
   1. CMYK
   2.
3. Additive color model (Computer industry) - Light theory.
   1. RGB
   2. rgb
1. Panton or Spot Color scheme for printing. 
![Light Theory](../../ILLUSTRATIONS/Light-theory.png)
1. Hue
   1. Red blue green yellow.  Pure color
1. Saturation
   1. Amount of grey added
   2. Chroma or intensity of the color
1. Based on the saturation of the color, they can be divided into
2. Tint - hue + white
3. Tone - hue + grey
4. Shades - hue + black

**`Hue` $\rightarrow$ `Tint` $\rightarrow$ `Tone` $\rightarrow$ `Shade`**

---

![Light Theory](../../ILLUSTRATIONS/colemot.png)

![Light Theory](../../ILLUSTRATIONS/webbanner.png)
