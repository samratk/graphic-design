### 1. 6 steps Design Process
1. Problem to be solved
2. Initial research
3. Brainstorm
4. Initial Design
5. Evaluation
6. Final design

### 2. Tips from Tony Fadell
1. There are invisible problems due to habituatio of human mind. Observe the problems and sovle them
2. Look Broader.
![look broader](ILLUSTRATIONS/look-broader.png)
3. Look Closer.
![look closer](ILLUSTRATIONS/look-closer.png)
4. Think younger - They are not habituated. They think out of the box. Have young people in your team. Stay beginner.
![Think younger](ILLUSTRATIONS/Think-Younger.png)

### 3. Kevin Betune
1. Have the x ray vision!
2. Build trust.
3. Shape shifting skills. (back pain story)
   1. Doctor traininig.
   2. Patient training.
1. Design for  hopes, fears and multiple senses.
2. 
